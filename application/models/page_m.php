<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	Class Page_m extends CI_Model{
		
		// users 
		function users()
		{
			$qry="select * from users order by id_users DESC";
			return $this->db->query($qry)->result();
		}
		
		// Create users
		function create_users($data)
		{	
			$this->db->insert('users', $data);			
			return;
		}
		
		// Users Update
		function users_update($id_users)
		{
			$qry="SELECT * FROM users where id_users='$id_users'";
			return $this->db->query($qry)->result();
		}
		
		// Users Update Process
		function users_update_process($id_users)
		{
			$data=array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'level' => $this->input->post('level'),
				'status' => $this->input->post('status')
			);
			
			$this->db->where('id_users', $id_users);
			$this->db->update('users', $data);
		}
		
		// Delete Users 
		function users_delete()
		{
			$this->db->where('id_users', $this->uri->segment(3));
			$this->db->delete('users');
		}
		
		// My account
		function myaccount()
		{
			$id_users=$this->session->userdata('id_users');
			$qry="SELECT * FROM users WHERE id_users='$id_users'";
			return $this->db->query($qry)->result();
		}
		
		// My Account
		function myaccount_update($id_users)
		{
			$qry="select * from users where id_users='$id_users'";
			return $this->db->query($qry)->result();
		}
		
		// My Account Update Process
		function myaccount_update_process($id_users)
		{
			
			$pass=md5($this->input->post('password'));
			$data=array(
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'level' => $this->input->post('level'),
				'status' => $this->input->post('status')
			);
			
			$this->db->where('id_users', $id_users);
			$this->db->update('users', $data);
		}
		
		

		
		function seo()
		{
			$qry="select * from seo";
			return $this->db->query($qry)->result();
		}
		
		
		// Create seo
		function create_seo($data)
		{
			$this->db->insert('seo', $data);
			return;
		}
		
		// seo Update
		function seo_update($id_seo)
		{
			$qry="select * from seo where id_seo='$id_seo'";
			return $this->db->query($qry)->result();
		}

		
		// seo update process
		function seo_update_process($id_seo){

			$data=array(
					'menu' => $this->input->post('menu'),
					'title' => $this->input->post('title'),
					'keyword' => $this->input->post('keyword'),
					'description' => $this->input->post('description')
				);
			$this->db->where('id_seo', $id_seo);
			$this->db->update('seo', $data);
			redirect('page/seo');
				
		}
		
		// Delete seo
		function seo_delete($id_seo)
		{
			$this->db->where('id_seo', $id_seo);
			$this->db->delete('seo');
		}
		
		
		
		
		// banner
		function banner()
		{
			$qry="SELECT * FROM banner order by id_banner DESC ";
			return $this->db->query($qry)->result();
		}
		
		// Create banner
		function create_banner($data)
		{
			$this->db->insert('banner', $data);
			return;
		}
		
		// banner Update
		function banner_update($id_banner)
		{
			$qry="select * from banner where id_banner='$id_banner'";
			return $this->db->query($qry)->result();
		}

		
		// banner update process
		function banner_update_process($id_banner){
			$this->load->library('image_lib');
			$url = './all_picture/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."banner/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				
				
				
			if (!empty($lokasi_file)){
	
				if($this->upload->do_upload('gambar'))
				{    
					$files = $this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
						$data=array(
							
							'title_banner' => $this->input->post('title_banner'),
							'image_banner' =>  $hasil
						);
					$res=mysql_query("SELECT image_banner FROM banner WHERE id_banner=".$id_banner);
					$row=mysql_fetch_array($res);
					unlink("all_picture/banner/" . $row['image_banner']);

					$this->db->where('id_banner', $id_banner);
					$this->db->update('banner', $data);
					redirect('page/banner');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
				
			}else{
				$data=array(
						
						'title_banner' => $this->input->post('title_banner')
					);
				$this->db->where('id_banner', $id_banner);
				$this->db->update('banner', $data);
				redirect('page/banner');
			}
		}
		
		// Delete banner
		function banner_delete($id_banner)
		{
			/*
			$this->db->where('id_banner', $this->uri->segment(3));
			$this->db->delete('banner');
			*/
			
			$res=mysql_query("SELECT image_banner FROM banner WHERE id_banner=".$id_banner);
			$row=mysql_fetch_array($res);
			unlink("all_picture/banner/" . $row['image_banner']);
			mysql_query("DELETE FROM banner WHERE id_banner=".$id_banner);
		}
		
		// social_media
		function social_media()
		{
			$qry="SELECT * FROM social_media order by id_social_media DESC ";
			return $this->db->query($qry)->result();
		}
		
		// Create social_media
		function create_social_media($data)
		{
			$this->db->insert('social_media', $data);
			return;
		}
		
		// social_media Update
		function social_media_update($id_social_media)
		{
			$qry="select * from social_media where id_social_media='$id_social_media'";
			return $this->db->query($qry)->result();
		}

		
		// social_media update process
		function social_media_update_process($id_social_media){
			$this->load->library('image_lib');
			$url = './all_picture/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."social_media/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				
			if (!empty($lokasi_file)){
	
				if( $this->upload->do_upload('gambar') )
				{    
					$files = $this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
						$data=array(
							'title_social_media' => $this->input->post('title_social_media'),
							'link_social_media' => $this->input->post('link_social_media'),
							'image_social_media' =>  $hasil
						);
						
					$res=mysql_query("SELECT image_social_media FROM social_media WHERE id_social_media=".$id_social_media);
					$row=mysql_fetch_array($res);
					unlink("all_picture/social_media/" . $row['image_social_media']);
					
					$this->db->where('id_social_media', $id_social_media);
					$this->db->update('social_media', $data);
					redirect('page/social_media');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
			}else{
				$data=array(
						'title_social_media' => $this->input->post('title_social_media'),
						'link_social_media' => $this->input->post('link_social_media')
					);
				$this->db->where('id_social_media', $id_social_media);
				$this->db->update('social_media', $data);
				redirect('page/social_media');
			}
		}
		
		// Delete social_media
		function social_media_delete($id_social_media)
		{
			/*
				$this->db->where('id_social_media', $this->uri->segment(3));
				$this->db->delete('social_media');
			*/
			
			$res=mysql_query("SELECT image_social_media FROM social_media WHERE id_social_media=".$id_social_media);
			$row=mysql_fetch_array($res);
			unlink("all_picture/social_media/" . $row['image_social_media']);
			mysql_query("DELETE FROM social_media WHERE id_social_media=".$id_social_media);
		}
		
		// projects_category
		function projects_category()
		{
			$qry="SELECT * FROM projects_category order by id_projects_category ASC";
			return $this->db->query($qry)->result();
		}
		
		// Create projects_category
		function create_projects_category($data)
		{
			$this->db->insert('projects_category', $data);
			return;
		}
		
		// projects_category Update
		function projects_category_update($id_projects_category)
		{
			$qry="select * from projects_category where id_projects_category='$id_projects_category'";
			return $this->db->query($qry)->result();
		}

		
		// projects_category update process
		function projects_category_update_process($id_projects_category){
			$title_projects_category_seo = seo($this->input->post('title_projects_category'));
			$data=array(
				'title_projects_category' => $this->input->post('title_projects_category'),
				'title_projects_category_seo' => $title_projects_category_seo,
				'publish' => $this->input->post('publish')
			);
			$this->db->where('id_projects_category', $id_projects_category);
			$this->db->update('projects_category', $data);
			redirect('page/projects_category');
				
		}
		
		// Delete projects_category
		function projects_category_delete()
		{
			$this->db->where('id_projects_category', $this->uri->segment(3));
			$this->db->delete('projects_category');
		}
		
		
		// projects
		function projects()
		{
			$qry="select a.id_projects,a.id_projects_category,a.title_projects,a.image_projects,
						b.id_projects_category,b.title_projects_category
						from projects a
						LEFT JOIN projects_category b on a.id_projects_category = b.id_projects_category
						order by a.id_projects DESC";
			return $this->db->query($qry)->result();
		}
		
		// Create projects
		function create_projects($data)
		{
			$this->db->insert('projects', $data);
			return;
		}
		
		// projects Update
		function projects_update($id_projects)
		{
			$qry="select * from projects where id_projects='$id_projects'";
			return $this->db->query($qry)->result();
		}

		
		// projects update process
		function projects_update_process($id_projects){
			$this->load->library('image_lib');
			$url = './all_picture/projects/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."original/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				$fileNameResize = $config['upload_path'].$files['file_name'];
				
			if (!empty($lokasi_file)){
	
				if( $this->upload->do_upload('gambar') )
				{    
					$files = $this->upload->data();
					
					$fileNameResize = $config['upload_path'].$files['file_name'];
					$size =  array(                
								array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
								array('name'    => 'medium','width'    => 250, 'height'    => 250, 'quality'    => '100%')
							);
					$resize = array();
					foreach($size as $r){                
						$resize = array(
							"width"            => $r['width'],
							"height"        => $r['height'],
							"quality"        => $r['quality'],
							"source_image"    => $fileNameResize,
							"new_image"        => $url.$r['name'].'/'.$files['file_name']
						);
						$this->image_lib->initialize($resize);
						if(!$this->image_lib->resize())                    
							die($this->image_lib->display_errors());
					}
			
				
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
					$title_projects_seo = seo($this->input->post('title_projects'));
					$data=array(	
						'id_projects_category' => $this->input->post('projects_category'),
						'title_projects' => $this->input->post('title_projects'),
						'title_projects_seo' => $title_projects_seo,
						'image_projects' =>  $hasil,
						/*
						'description' => $this->input->post('description'),
						'status' => $this->input->post('status'),
						'location' => $this->input->post('location'),
						'team' => $this->input->post('team'),
						'photographer' => $this->input->post('photographer'),
						*/
						'all_image' => $this->input->post('all_image'),
						'journal' => $this->input->post('journal'),
						/*
						'awards' => $this->input->post('awards'),
						*/
						'meta_title' => $this->input->post('meta_title'),
						'meta_keywords' => $this->input->post('meta_keywords'),
						'meta_description' => $this->input->post('meta_description'),
						'date_projects' => $this->input->post('date_projects')
					);
					
					$res=mysql_query("SELECT image_projects FROM projects WHERE id_projects=".$id_projects);
					$row=mysql_fetch_array($res);
					unlink("all_picture/projects/original/" . $row['image_projects']);
					unlink("all_picture/projects/medium/" . $row['image_projects']);
					unlink("all_picture/projects/small/" . $row['image_projects']);
			
					$this->db->where('id_projects', $id_projects);
					$this->db->update('projects', $data);
					redirect('page/projects');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
			}else{
				$title_projects_seo = seo($this->input->post('title_projects'));
				$data=array(	
					'id_projects_category' => $this->input->post('projects_category'),
					'title_projects' => $this->input->post('title_projects'),
					'title_projects_seo' => $title_projects_seo,
					/*
					'description' => $this->input->post('description'),
					'status' => $this->input->post('status'),
					'location' => $this->input->post('location'),
					'team' => $this->input->post('team'),
					'photographer' => $this->input->post('photographer'),
					*/
					'all_image' => $this->input->post('all_image'),
					'journal' => $this->input->post('journal'),
					/*
					'awards' => $this->input->post('awards'),
					*/
					'meta_title' => $this->input->post('meta_title'),
					'meta_keywords' => $this->input->post('meta_keywords'),
					'meta_description' => $this->input->post('meta_description'),
					'date_projects' => $this->input->post('date_projects')
				);
				$this->db->where('id_projects', $id_projects);
				$this->db->update('projects', $data);
				redirect('page/projects');
			}
		}
		
		// Delete projects
		function projects_delete($id_projects)
		{
			/*
			$this->db->where('id_projects', $this->uri->segment(3));
			$this->db->delete('projects');
			*/
			
			$res=mysql_query("SELECT image_projects FROM projects WHERE id_projects=".$id_projects);
			$row=mysql_fetch_array($res);
			unlink("all_picture/projects/original/" . $row['image_projects']);
			unlink("all_picture/projects/medium/" . $row['image_projects']);
			unlink("all_picture/projects/small/" . $row['image_projects']);
			mysql_query("DELETE FROM projects WHERE id_projects=".$id_projects);
		}
		
		// news_category
		function news_category()
		{
			$qry="SELECT * FROM news_category order by id_news_category ASC";
			return $this->db->query($qry)->result();
		}
		
		// Create news_category
		function create_news_category($data)
		{
			$this->db->insert('news_category', $data);
			return;
		}
		
		// news_category Update
		function news_category_update($id_news_category)
		{
			$qry="select * from news_category where id_news_category='$id_news_category'";
			return $this->db->query($qry)->result();
		}

		
		// news_category update process
		function news_category_update_process($id_news_category){
			$title_news_category_seo = seo($this->input->post('title_news_category'));
			$data=array(
				'title_news_category' => $this->input->post('title_news_category'),
				'title_news_category_seo' => $title_news_category_seo,
				'publish' => $this->input->post('publish')
			);
			$this->db->where('id_news_category', $id_news_category);
			$this->db->update('news_category', $data);
			redirect('page/news_category');
				
		}
		
		// Delete news_category
		function news_category_delete()
		{
			$this->db->where('id_news_category', $this->uri->segment(3));
			$this->db->delete('news_category');
		}
		
		// news
		function news()
		{
			$qry="select a.id_news,a.id_news_category,a.title_news,a.image_news,
						b.id_news_category,b.title_news_category
						from news a
						LEFT JOIN news_category b on a.id_news_category = b.id_news_category
						order by a.id_news DESC";
			return $this->db->query($qry)->result();
		}
		
		
		
		
		// Create news
		function create_news($data)
		{
			$this->db->insert('news', $data);
			return;
		}
		
		// news Update
		function news_update($id_news)
		{
			$qry="select * from news where id_news='$id_news'";
			return $this->db->query($qry)->result();
		}

		
		// news update process
		function news_update_process($id_news){
			$this->load->library('image_lib');
			$url = './all_picture/news/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."original/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				$fileNameResize = $config['upload_path'].$files['file_name'];
				
			if (!empty($lokasi_file)){
	
				if( $this->upload->do_upload('gambar') )
				{    
					$files = $this->upload->data();
					
					$fileNameResize = $config['upload_path'].$files['file_name'];
					$size =  array(                
								array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
								array('name'    => 'medium','width'    => 500, 'height'    => 500, 'quality'    => '100%')
							);
					$resize = array();
					foreach($size as $r){                
						$resize = array(
							"width"            => $r['width'],
							"height"        => $r['height'],
							"quality"        => $r['quality'],
							"source_image"    => $fileNameResize,
							"new_image"        => $url.$r['name'].'/'.$files['file_name']
						);
						$this->image_lib->initialize($resize);
						if(!$this->image_lib->resize())                    
							die($this->image_lib->display_errors());
					}

					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
					$title_news_seo = seo($this->input->post('title_news'));
					$data=array(	
						'id_news_category' => $this->input->post('news_category'),
						'title_news' => $this->input->post('title_news'),
						'title_news_seo' => $title_news_seo,
						'image_news' =>  $hasil,
						'description' => $this->input->post('description'),
						'meta_title' => $this->input->post('meta_title'),
						'meta_keywords' => $this->input->post('meta_keywords'),
						'meta_description' => $this->input->post('meta_description')
					);
					
					$res=mysql_query("SELECT image_news FROM news WHERE id_news=".$id_news);
					$row=mysql_fetch_array($res);
					unlink("all_picture/news/original/" . $row['image_news']);
					unlink("all_picture/news/medium/" . $row['image_news']);
					unlink("all_picture/news/small/" . $row['image_news']);
					
					$this->db->where('id_news', $id_news);
					$this->db->update('news', $data);
					redirect('page/news');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
			}else{
				$title_news_seo = seo($this->input->post('title_news'));
				$data=array(	
					'id_news_category' => $this->input->post('news_category'),
					'title_news' => $this->input->post('title_news'),
					'title_news_seo' => $title_news_seo,
					'description' => $this->input->post('description'),
					'meta_title' => $this->input->post('meta_title'),
					'meta_keywords' => $this->input->post('meta_keywords'),
					'meta_description' => $this->input->post('meta_description')
				);
				$this->db->where('id_news', $id_news);
				$this->db->update('news', $data);
				redirect('page/news');
			}
		}
		
		// Delete news
		function news_delete($id_news)
		{
			/*
			$this->db->where('id_news', $id_news);
			$this->db->delete('news');
			*/
			
			$res=mysql_query("SELECT image_news FROM news WHERE id_news=".$id_news);
			$row=mysql_fetch_array($res);
			unlink("all_picture/news/original/" . $row['image_news']);
			unlink("all_picture/news/medium/" . $row['image_news']);
			unlink("all_picture/news/small/" . $row['image_news']);
			
			mysql_query("DELETE FROM news WHERE id_news=".$id_news);
			
		}
		
		
		// contact
		function contact()
		{
			$qry="SELECT * FROM contact order by id_contact ASC";
			return $this->db->query($qry)->result();
		}
		
		// Create contact
		function create_contact($data)
		{
			$this->db->insert('contact', $data);
			return;
		}
		
		// contact Update
		function contact_update($id_contact)
		{
			$qry="select * from contact where id_contact='$id_contact'";
			return $this->db->query($qry)->result();
		}

		
		// contact update process
		function contact_update_process($id_contact){

			$data=array(
				'nation' => $this->input->post('nation'),
				'address' => $this->input->post('address'),
				'telp' => $this->input->post('telp'),
				'fax' => $this->input->post('fax'),
				'email' => $this->input->post('email'),
				'map' => $this->input->post('map')
				);
			$this->db->where('id_contact', $id_contact);
			$this->db->update('contact', $data);
			redirect('page/contact');
				
		}
		
		// Delete contact
		function contact_delete()
		{
			$this->db->where('id_contact', $this->uri->segment(3));
			$this->db->delete('contact');
		}
		
		
		// about
		function about()
		{
			$qry="SELECT * FROM about order by id_about ASC";
			return $this->db->query($qry)->result();
		}
		// about_bannner
		function about_banner()
		{
			$qry="SELECT * FROM about_banner where id_about_banner='1'";
			return $this->db->query($qry)->result();
		}
		
		// Create about
		function create_about($data)
		{
			$this->db->insert('about', $data);
			return;
		}
		
		// about Update
		function about_update($id_about)
		{
			$qry="select * from about where id_about='$id_about'";
			return $this->db->query($qry)->result();
		}

		
		// about update process
		function about_update_process($id_about){

			$this->load->library('image_lib');
			$url = './all_picture/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."about/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				
				
				
			if (!empty($lokasi_file)){
	
				if($this->upload->do_upload('gambar'))
				{    
					$files = $this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
					$title_seo = seo($this->input->post('title'));
						$data=array(
							'title' => $this->input->post('title'),
							'title_seo' => $title_seo,
							'description' => $this->input->post('description'),
							'image_about' =>  $hasil
						);
					$res=mysql_query("SELECT image_about FROM about WHERE id_about=".$id_about);
					$row=mysql_fetch_array($res);
					unlink("all_picture/about/" . $row['image_about']);
					
					$this->db->where('id_about', $id_about);
					$this->db->update('about', $data);
					redirect('page/about');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
				
			}else{
				$title_seo = seo($this->input->post('title'));
				$data=array(
					'title' => $this->input->post('title'),
					'title_seo' => $title_seo,
					'description' => $this->input->post('description')
				);
				$this->db->where('id_about', $id_about);
				$this->db->update('about', $data);
				redirect('page/about');
			}
				
		}
		
		// Delete about
		function about_delete($id_about)
		{
			$res=mysql_query("SELECT image_about FROM about WHERE id_about=".$id_about);
			$row=mysql_fetch_array($res);
			unlink("all_picture/about/" . $row['image_about']);
			mysql_query("DELETE FROM about WHERE id_about=".$id_about);
		}
		
		
		
		
		// services
		function services()
		{
			$qry="SELECT * FROM services order by id_services DESC";
			return $this->db->query($qry)->result();
		}
		
		// Create services
		function create_services($data)
		{
			$this->db->insert('services', $data);
			return;
		}
		
		// services Update
		function services_update($id_services)
		{
			$qry="select * from services where id_services='$id_services'";
			return $this->db->query($qry)->result();
		}

		
		// services update process
		function services_update_process($id_services){

			$data=array(
				'title' => $this->input->post('title'),
				'description' => $this->input->post('description')
				);
			$this->db->where('id_services', $id_services);
			$this->db->update('services', $data);
			redirect('page/services');
				
		}
		
		// Delete services
		function services_delete()
		{
			$this->db->where('id_services', $this->uri->segment(3));
			$this->db->delete('services');
		}

// journal
		function journal()
		{
			$qry="select a.id_journal,a.id_projects_category,a.title_projects,a.image_projects,a.journal,
						b.id_projects_category,b.title_projects_category
						from journal a
						LEFT JOIN projects_category b on a.id_projects_category = b.id_projects_category
						order by a.id_journal DESC";
			return $this->db->query($qry)->result();
		}
		
		// Create journal
		function create_journal($data)
		{
			$this->db->insert('journal', $data);
			return;
		}
		
		// journal Update
		function journal_update($id_journal)
		{
			$qry="select * from journal where id_journal='$id_journal'";
			return $this->db->query($qry)->result();
		}

		
		// journal update process
		function journal_update_process($id_journal){
			$this->load->library('image_lib');
			$url = './all_picture/journal/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."original/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				$fileNameResize = $config['upload_path'].$files['file_name'];
				
			if (!empty($lokasi_file)){
	
				if( $this->upload->do_upload('gambar') )
				{    
					$files = $this->upload->data();
					
					$fileNameResize = $config['upload_path'].$files['file_name'];
					$size =  array(                
								array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
								array('name'    => 'medium','width'    => 250, 'height'    => 250, 'quality'    => '100%')
							);
					$resize = array();
					foreach($size as $r){                
						$resize = array(
							"width"            => $r['width'],
							"height"        => $r['height'],
							"quality"        => $r['quality'],
							"source_image"    => $fileNameResize,
							"new_image"        => $url.$r['name'].'/'.$files['file_name']
						);
						$this->image_lib->initialize($resize);
						if(!$this->image_lib->resize())                    
							die($this->image_lib->display_errors());
					}
			
				
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
					$title_projects_seo = seo($this->input->post('title_projects'));
					$data=array(	
						'id_projects_category' => $this->input->post('projects_category'),
						'title_projects' => $this->input->post('title_projects'),
						'title_projects_seo' => $title_projects_seo,
						'image_projects' =>  $hasil,
						/*
						'description' => $this->input->post('description'),
						'status' => $this->input->post('status'),
						'location' => $this->input->post('location'),
						'team' => $this->input->post('team'),
						'photographer' => $this->input->post('photographer'),
						*/
						'all_image' => $this->input->post('all_image'),
						'journal' => $this->input->post('journal'),
						/*
						'awards' => $this->input->post('awards'),
						*/
						'meta_title' => $this->input->post('meta_title'),
						'meta_keywords' => $this->input->post('meta_keywords'),
						'meta_description' => $this->input->post('meta_description'),
						'date_projects' => $this->input->post('date_projects')
					);
					
					$res=mysql_query("SELECT image_projects FROM journal WHERE id_journal=".$id_journal);
					$row=mysql_fetch_array($res);
					unlink("all_picture/journal/original/" . $row['image_projects']);
					unlink("all_picture/journal/medium/" . $row['image_projects']);
					unlink("all_picture/journal/small/" . $row['image_projects']);
			
					$this->db->where('id_journal', $id_journal);
					$this->db->update('journal', $data);
					redirect('page/journal');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
			}else{
				$title_projects_seo = seo($this->input->post('title_projects'));
				$data=array(	
					'id_projects_category' => $this->input->post('projects_category'),
					'title_projects' => $this->input->post('title_projects'),
					'title_projects_seo' => $title_projects_seo,
					/*
					'description' => $this->input->post('description'),
					'status' => $this->input->post('status'),
					'location' => $this->input->post('location'),
					'team' => $this->input->post('team'),
					'photographer' => $this->input->post('photographer'),
					*/
					'all_image' => $this->input->post('all_image'),
					'journal' => $this->input->post('journal'),
					/*
					'awards' => $this->input->post('awards'),
					*/
					'meta_title' => $this->input->post('meta_title'),
					'meta_keywords' => $this->input->post('meta_keywords'),
					'meta_description' => $this->input->post('meta_description'),
					'date_projects' => $this->input->post('date_projects')
				);
				$this->db->where('id_journal', $id_journal);
				$this->db->update('journal', $data);
				redirect('page/journal');
			}
		}
		
		// Delete journal
		function journal_delete($id_journal)
		{
			/*
			$this->db->where('id_journal', $this->uri->segment(3));
			$this->db->delete('journal');
			*/
			
			$res=mysql_query("SELECT image_projects FROM journal WHERE id_journal=".$id_journal);
			$row=mysql_fetch_array($res);
			unlink("all_picture/journal/original/" . $row['image_projects']);
			unlink("all_picture/journal/medium/" . $row['image_projects']);
			unlink("all_picture/journal/small/" . $row['image_projects']);
			mysql_query("DELETE FROM journal WHERE id_journal=".$id_journal);
		}
		
		
		
		
		
		
		
		// logo
		function logo()
		{
			$qry="SELECT * FROM template where id_template='1' ";
			return $this->db->query($qry)->result();
		}
		
		
		// logo Update
		function logo_update($id_template)
		{
			$qry="select * from template where id_template='$id_template'";
			return $this->db->query($qry)->result();
		}

		
		// logo update process
		function logo_update_process($id_template){
			$this->load->library('image_lib');
			$url = './all_picture/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."template/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				
			if (!empty($lokasi_file)){
	
				if( $this->upload->do_upload('gambar') )
				{    
					$files = $this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
						$data=array(
							'title' => $this->input->post('title'),
							'img_template' =>  $hasil
						);
						
					$res=mysql_query("SELECT img_template FROM template WHERE id_template=".$id_template);
					$row=mysql_fetch_array($res);
					unlink("all_picture/template/" . $row['img_template']);
					
					$this->db->where('id_template', $id_template);
					$this->db->update('template', $data);
					redirect('page/logo');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
			}else{
				$data=array(
						'title' => $this->input->post('title'),
					);
				$this->db->where('id_template', $id_template);
				$this->db->update('template', $data);
				redirect('page/logo');
			}
		}
		
		
		
		// about_banner Update
		function about_banner_update($id_about_banner)
		{
			$qry="select * from about_banner where id_about_banner='$id_about_banner'";
			return $this->db->query($qry)->result();
		}

		
		// about_banner update process
		function about_banner_update_process($id_about_banner){
			$this->load->library('image_lib');
			$url = './all_picture/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."about_banner/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				
				
				
			if (!empty($lokasi_file)){
	
				if($this->upload->do_upload('gambar'))
				{    
					$files = $this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
					
						$data=array(
							'title_about_banner' => $this->input->post('title_about_banner'),
							'image_about_banner' =>  $hasil,
							'description' => $this->input->post('description')
						);
					$res=mysql_query("SELECT image_about_banner FROM about_banner WHERE id_about_banner=".$id_about_banner);
					$row=mysql_fetch_array($res);
					unlink("all_picture/about_banner/" . $row['image_about_banner']);
					
					$this->db->where('id_about_banner', $id_about_banner);
					$this->db->update('about_banner', $data);
					redirect('page/about');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
				
			}else{
				$data=array(
					'title_about_banner' => $this->input->post('title_about_banner'),
					'description' => $this->input->post('description')
				);
				$this->db->where('id_about_banner', $id_about_banner);
				$this->db->update('about_banner', $data);
				redirect('page/about');
			}
				
		}






		// designers
		function designers()
		{
			$qry="SELECT * FROM designers order by id_designers ASC";
			return $this->db->query($qry)->result();
		}
		
		// Create designers
		function create_designers($data)
		{
			$this->db->insert('designers', $data);
			return;
		}
		
		// designers Update
		function designers_update($id_designers)
		{
			$qry="select * from designers where id_designers='$id_designers'";
			return $this->db->query($qry)->result();
		}

		
		// designers update process
		function designers_update_process($id_designers){

			$this->load->library('image_lib');
			$url = './all_picture/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."designers/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				
				
				
			if (!empty($lokasi_file)){
	
				if($this->upload->do_upload('gambar'))
				{    
					$files = $this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
						$data=array(
							'title' => $this->input->post('title'),
							'description' => $this->input->post('description'),
							'image_designers' =>  $hasil
						);
					$res=mysql_query("SELECT image_designers FROM designers WHERE id_designers=".$id_designers);
					$row=mysql_fetch_array($res);
					unlink("all_picture/designers/" . $row['image_designers']);
					
					$this->db->where('id_designers', $id_designers);
					$this->db->update('designers', $data);
					redirect('page/designers');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
				
			}else{
				
				$data=array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description')
				);
				$this->db->where('id_designers', $id_designers);
				$this->db->update('designers', $data);
				redirect('page/designers');
			}
				
		}
		
		// Delete designers
		function designers_delete($id_designers)
		{
			$res=mysql_query("SELECT image_designers FROM designers WHERE id_designers=".$id_designers);
			$row=mysql_fetch_array($res);
			unlink("all_picture/designers/" . $row['image_designers']);
			mysql_query("DELETE FROM designers WHERE id_designers=".$id_designers);
		}
		

		// blogs_category
		function blogs_category()
		{
			$qry="SELECT * FROM blogs_category order by id_blogs_category ASC";
			return $this->db->query($qry)->result();
		}
		
		// Create blogs_category
		function create_blogs_category($data)
		{
			$this->db->insert('blogs_category', $data);
			return;
		}
		
		// blogs_category Update
		function blogs_category_update($id_blogs_category)
		{
			$qry="select * from blogs_category where id_blogs_category='$id_blogs_category'";
			return $this->db->query($qry)->result();
		}

		
		// blogs_category update process
		function blogs_category_update_process($id_blogs_category){
			$title_blogs_category_seo = seo($this->input->post('title_blogs_category'));
			$data=array(
				'title_blogs_category' => $this->input->post('title_blogs_category'),
				'title_blogs_category_seo' => $title_blogs_category_seo,
				'publish' => $this->input->post('publish')
			);
			$this->db->where('id_blogs_category', $id_blogs_category);
			$this->db->update('blogs_category', $data);
			redirect('page/blogs_category');
				
		}
		
		// Delete blogs_category
		function blogs_category_delete()
		{
			$this->db->where('id_blogs_category', $this->uri->segment(3));
			$this->db->delete('blogs_category');
		}
		
		// blogs
		function blogs()
		{
			$qry="select a.id_blogs,a.id_blogs_category,a.title_blogs,a.image_blogs,
						b.id_blogs_category,b.title_blogs_category
						from blogs a
						LEFT JOIN blogs_category b on a.id_blogs_category = b.id_blogs_category
						order by a.id_blogs DESC";
			return $this->db->query($qry)->result();
		}
		
		
		
		
		// Create blogs
		function create_blogs($data)
		{
			$this->db->insert('blogs', $data);
			return;
		}
		
		// blogs Update
		function blogs_update($id_blogs)
		{
			$qry="select * from blogs where id_blogs='$id_blogs'";
			return $this->db->query($qry)->result();
		}

		
		// blogs update process
		function blogs_update_process($id_blogs){
			$this->load->library('image_lib');
			$url = './all_picture/blogs/';    //path image
			if($_FILES['gambar']['error']==3)  //if No file was uploaded.
			return false;        
				
				$config['upload_path'] = $url."original/";        
				$config['allowed_types'] = 'jpg|png|jpeg';
				$config['max_size']        = 2048; 
				$lokasi_file    = $_FILES['gambar']['tmp_name'];
				
				
				$this->load->library('upload');
				$this->upload->initialize($config);
				
				$files = $this->upload->data();
				$fileNameResize = $config['upload_path'].$files['file_name'];
				
			if (!empty($lokasi_file)){
	
				if( $this->upload->do_upload('gambar') )
				{    
					$files = $this->upload->data();
					
					$fileNameResize = $config['upload_path'].$files['file_name'];
					$size =  array(                
								array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
								array('name'    => 'medium','width'    => 500, 'height'    => 500, 'quality'    => '100%')
							);
					$resize = array();
					foreach($size as $r){                
						$resize = array(
							"width"            => $r['width'],
							"height"        => $r['height'],
							"quality"        => $r['quality'],
							"source_image"    => $fileNameResize,
							"new_image"        => $url.$r['name'].'/'.$files['file_name']
						);
						$this->image_lib->initialize($resize);
						if(!$this->image_lib->resize())                    
							die($this->image_lib->display_errors());
					}

					$data = array('upload_data' => $this->upload->data());
					$hasil = str_replace (" ",  "_", $files['file_name']);
					$title_blogs_seo = seo($this->input->post('title_blogs'));
					$data=array(	
						'id_blogs_category' => $this->input->post('blogs_category'),
						'title_blogs' => $this->input->post('title_blogs'),
						'title_blogs_seo' => $title_blogs_seo,
						'image_blogs' =>  $hasil,
						'description' => $this->input->post('description'),
						'meta_title' => $this->input->post('meta_title'),
						'meta_keywords' => $this->input->post('meta_keywords'),
						'meta_description' => $this->input->post('meta_description')
					);
					
					$res=mysql_query("SELECT image_blogs FROM blogs WHERE id_blogs=".$id_blogs);
					$row=mysql_fetch_array($res);
					unlink("all_picture/blogs/original/" . $row['image_blogs']);
					unlink("all_picture/blogs/medium/" . $row['image_blogs']);
					unlink("all_picture/blogs/small/" . $row['image_blogs']);
					
					$this->db->where('id_blogs', $id_blogs);
					$this->db->update('blogs', $data);
					redirect('page/blogs');
				}
				else{
					$this->load->view('backend/image_validasi_v');
				}
			}else{
				$title_blogs_seo = seo($this->input->post('title_blogs'));
				$data=array(	
					'id_blogs_category' => $this->input->post('blogs_category'),
					'title_blogs' => $this->input->post('title_blogs'),
					'title_blogs_seo' => $title_blogs_seo,
					'description' => $this->input->post('description'),
					'meta_title' => $this->input->post('meta_title'),
					'meta_keywords' => $this->input->post('meta_keywords'),
					'meta_description' => $this->input->post('meta_description')
				);
				$this->db->where('id_blogs', $id_blogs);
				$this->db->update('blogs', $data);
				redirect('page/blogs');
			}
		}
		
		// Delete blogs
		function blogs_delete($id_blogs)
		{
			/*
			$this->db->where('id_blogs', $id_blogs);
			$this->db->delete('blogs');
			*/
			
			$res=mysql_query("SELECT image_blogs FROM blogs WHERE id_blogs=".$id_blogs);
			$row=mysql_fetch_array($res);
			unlink("all_picture/blogs/original/" . $row['image_blogs']);
			unlink("all_picture/blogs/medium/" . $row['image_blogs']);
			unlink("all_picture/blogs/small/" . $row['image_blogs']);
			
			mysql_query("DELETE FROM blogs WHERE id_blogs=".$id_blogs);
			
		}





		// careers
		function careers()
		{
			$qry="SELECT * FROM careers order by id_careers ASC";
			return $this->db->query($qry)->result();
		}
		
		// Create careers
		function create_careers($data)
		{
			$this->db->insert('careers', $data);
			return;
		}
		
		// careers Update
		function careers_update($id_careers)
		{
			$qry="select * from careers where id_careers='$id_careers'";
			return $this->db->query($qry)->result();
		}

		
		// careers update process
		function careers_update_process($id_careers){

			$data=array(
				'position' => $this->input->post('position'),
				'description' => $this->input->post('description')
				);
			$this->db->where('id_careers', $id_careers);
			$this->db->update('careers', $data);
			redirect('page/careers');
				
		}
		
		// Delete careers
		function careers_delete()
		{
			$this->db->where('id_careers', $this->uri->segment(3));
			$this->db->delete('careers');
		}
		
	}
	
?>