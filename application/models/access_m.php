<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	Class Access_m extends CI_Model{
		
		// journal limit
		function journalLimit($count){
			$this->db->limit($count);
			$this->db->order_by('id_journal','desc');
			return $this->db->get('journal');
		}

		// journalSelected
		function journalSelected($id){
			$this->db->where('id_journal',$id);

			return $this->db->get('journal');
		}

		// journal all
		function journalAll(){
			return $this->db->get('journal');
		}

		// journalOrderLimit
		function journalOrderLimit($limit, $order){
			$this->db->order_by('id_journal', 'desc');
			$this->db->limit($limit, $order);

			return $this->db->get('journal');
		}

		// all designer
		function getAllDesigner(){

			$this->db->order_by('id_designers', 'desc');
			return $this->db->get('designers');
		}

		// get news category
		function getNewsCategory(){
			return $this->db->get('news_category');
		}

		// get data news category
		function getDataNewsCategory($id){
			$this->db->where('id_news_category', $id);
			$this->db->order_by('id_news', 'desc');

			return $this->db->get('news');
		}

		// get news selected
		function getNewsSelected($id){
			$this->db->where('id_news', $id);
			$this->db->order_by('id_news', 'desc');

			return $this->db->get('news');
		}

		// get works
		function getWorks(){
			$this->db->order_by('id_projects', 'desc');
			return $this->db->get('projects');
		}

		// get work category
		function getWorkCategory(){
			$this->db->order_by('id_projects_category', 'desc');
			
			return $this->db->get('projects_category');
		}

		// get works selected
		function getWorksSelected($id){
			$this->db->where('id_projects', $id);

			return $this->db->get('projects');
		}

		// get contact
		function getContact(){

			$this->db->order_by('id_contact', 'desc');

			return $this->db->get('contact');
		}

		// get career
		function getCareer(){
			$this->db->order_by('id_careers', 'desc');

			return $this->db->get('careers');
		}

		// get about
		function getAbout(){
			$this->db->order_by('id_about', 'desc');

			return $this->db->get('about');
		}

		// about banner
		function about_banner(){
			return $this->db->get('about_banner');
		}

		// main banner
		function getMainBanner(){
			return $this->db->get('banner');
		}

		// get logo
		function getLogo(){
			return $this->db->get('template');
		}

		// get blog
		function getBlog(){
			$this->db->order_by('id_blogs', 'desc');

			return $this->db->get('blogs');
		}

		// blog order limit
		function blogOrderLimit($limit, $order){
			$this->db->order_by('id_blogs', 'desc');
			$this->db->limit($limit, $order);

			return $this->db->get('blogs');
		}

		// journal selected
		function blogSelected($id){
			$this->db->where('id_blogs',$id);

			return $this->db->get('blogs');
		}
	}
	
?>