<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">

		<!-- content journal -->
		<?php
		foreach($blogSelected->result() as $data){ ?>
			<section class="content-journal">
				<h3><?php echo $data->title_blogs ?></h3>
				<p><?php echo $data->created_date ?></p>
				<section class="journal10">
					<img class="main_image_content" src="<?php echo base_url(); ?>all_picture/blogs/original/<?php echo $data->image_blogs ?>" alt="<?php echo $data->image_blogs ?>">
				</section>
				<div class="description_content">
					<?php echo $data->description ?>
				</div>
			</section>
		<?php
		}
		?>

	</article>

	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>