<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article>

		<!-- carousel -->
		<section class="box-carousel">
			<?php
			foreach($about_banner->result() as $data){ ?>

				<img src="<?php echo base_url(); ?>all_picture/about_banner/<?php echo $data->image_about_banner ?>" alt="main-room"> <?php

			}
			?>
		</section>

		<?php
		$no = 1;
		foreach($about->result() as $data){
			if($no % 2 == 0){ ?>

				<section class="wrapper-about about-to-top">
					<ul>
						<li class="about-info dekstop_designer">
							<div>	
								<h3><?php echo $data->title ?></h3>
								<hr align="left">
								<p>
									<?php echo $data->description ?>
								</p>
							</div>
						</li>
						<li class="about-img">
							<img src="<?php echo base_url(); ?>all_picture/about/<?php echo $data->image_about ?>" alt="<?php echo $data->image_about ?>">
						</li>
						<li class="about-info mobile">
							<div>	
								<h3><?php echo $data->title ?></h3>
								<hr align="left">
								<p>
									<?php echo $data->description ?>
								</p>
							</div>
						</li>
					</ul>
				</section> <?php

			}else{ ?>

				<section class="wrapper-about">
					<ul>
						<li class="about-img">
							<img src="<?php echo base_url(); ?>all_picture/about/<?php echo $data->image_about ?>" alt="<?php echo $data->image_about ?>">
						</li>
						<li class="about-info">
							<div>	
								<h3><?php echo $data->title ?></h3>
								<hr align="left">
								<p>
									<?php echo $data->description ?>
								</p>
							</div>
						</li>
					</ul>
				</section> <?php

			}

			$no++;
		}
		?>

	</article>

	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>