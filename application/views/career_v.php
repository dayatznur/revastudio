<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">
		<section class="main-work">
			<h3 class="title_page">CAREER</h3>
			<div class="container_career">
				
				<?php
				foreach($career->result() as $data){ ?>

					<div class="box_career">
						<div class="content_grid_career">
							<h4><?php echo $data->position ?></h4>
							<div class="box_desc_career">
								<?php echo $data->description ?>
							</div>
						</div>
					</div> <?php

				}
				?>

			</div>
			<div class="box_email">
				<div>
					<h4>Please send application to: <a href="mailto: career@revastudio.com">career@revastudio.com</a></h4>
				</div>
			</div>
		</section>
	</article>
	
	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>