<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">
		<section class="main-news">
			<section class="content-news">
				<h3>PUBLICATIONS 1 TITLE</h3>
				<p>5 February 2016</p>
				<section><a href="#"><img src="<?php echo base_url(); ?>assets/img/building-view-corner.jpg" alt="building-view-corner"></a></section>
				<div class="description_content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.
					<br><br>
					Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.
				</div>
			</section>
		</section>
	</article>
	
	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>