<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">
		<section class="main-designer">
			<h3>DESIGNERS</h3>
			
			<?php
			$no = 1;
			foreach($designer->result() as $data){
				if($no % 2 == 0){ ?>

					<section class="item-designer">
						<section class="designer-info unique-info-designer dekstop_designer">
							<div>
								<h3><?php echo $data->title ?></h3>
								<hr align="left">
								<p>
									<?php echo $data->description ?>
								</p>
							</div>
						</section>
						<section class="designer-img"><img src="<?php echo base_url(); ?>all_picture/designers/<?php echo $data->image_designers ?>" alt="Copenhagen"></section>
						<section class="designer-info unique-info-designer mobile">
							<div>
								<h3><?php echo $data->title ?></h3>
								<hr align="left">
								<p>
									<?php echo $data->description ?>
								</p>
							</div>
						</section>
					</section> <?php

				}else{ ?>

					<section class="item-designer">
						<section class="designer-img"><img src="<?php echo base_url(); ?>all_picture/designers/<?php echo $data->image_designers ?>" alt="white-house"></section>
						<section class="designer-info">
							<div>
								<h3><?php echo $data->title ?></h3>
								<hr align="left">
								<p>
									<?php echo $data->description ?>
								</p>
							</div>
						</section>
					</section> <?php

				}

				$no++;
			}
			?>

		</section>
	</article>

	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>