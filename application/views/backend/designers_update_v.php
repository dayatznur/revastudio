<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
               <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Designers <small>Update</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-cogs"></i> Designers
                            </li>
                        </ol>
                    </div>
                </div>
				
				<div class="row">
					<?php foreach($designers_update as $data): ?>
					<?php echo form_open_multipart('page/designers_update_process/'.$data->id_designers, 'onsubmit="return ValidationAboutUpdate()"'); ?>
					<div class="col-lg-6">
						<div class="form-group">
							<p>Name</p>
							<input type="text" id="title" name="title" value="<?php echo $data->title ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Description</p>
							<textarea name="description" class="form-control" id="editor"><?php echo $data->description ?></textarea>
						</div>
						<div class="form-group">
							<p>Image</p>
							<div class="alert alert-info" style="padding:8px;">
							  <strong>Info!</strong> Max size 2 MB and format file jpg or png
							</div>
							<p><img src="<?php echo base_url(); ?>all_picture/designers/<?php echo $data->image_designers ?>" style="width:220px;"></p> 
							
							<input type="file" name="gambar">
						</div>
						<input type="submit" value="Save" class="btn btn-success" style="width:100px;">	
					</div>
					</form>
				</div>
				
				<?php endforeach ?>	
            </div>
        </div>
		
    </div>

	<?php $this->load->view('backend/footer_v'); ?>
	
</body>
</html>
<?php
	}
?>
