<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
   
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Projects <small>Create</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> Projects
                            </li>
                        </ol>
                    </div>
                </div>
				
				
				
				<?php echo form_open_multipart('page/create_projects_process', 'onsubmit="return ValidationProjects()"'); ?>
				
<?php 
					$query = mysql_query("select * from projects 
						order by order_no DESC limit 1");
						while($q=mysql_fetch_array($query)){
						$v = $q['order_no']+1;
					}
					
				?>
				
				<input type="hidden" name="order_no" value="<?php echo $v;?>">
				<div class="row">
					
					<div class="col-lg-8">
				
						<div class="form-group">
						<p>Projects Category <span style="color:red; float:right;">(*) Must be Filled</span></p>
						<select id="projects_category" name="projects_category" style="margin-top:10px;" class="form-control">
							<option value="">- Select -</option>
							<?php foreach($projects_category as $data): ?>
								<?php
									if($data->publish == 1){
										?>
										<option value=<?php echo $data->id_projects_category ?>><?php echo $data->title_projects_category ?></option>
										<?php
									}
								?>
								
									
							<?php endforeach ?>		
							
						</select> 
						</div>
						
						<div class="form-group">
							<p>Title  <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<input type="text" id="title_projects" name="title_projects" class="form-control">
						</div>

						<div class="form-group">
							<p>Date </p>
							<input type="text" name="date_projects" placeholder="0000-00-00" id="dp1" class="form-control">
						</div>

						

						<div class="form-group">
							<p>Image  <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<div class="alert alert-info" style="padding:8px;">
							  <strong>Info!</strong> Max size 2 MB and format file jpg or png
							</div>
							<input type="file" name="gambar" id="gambar">
						</div>

						<!--
						<div class="form-group">
							<p>Description</p>
							<textarea name="description" class="form-control" id="editor"></textarea>
						</div>
						

						<div class="form-group">
							<p>Status</p>
							<input type="text" name="status" class="form-control">
						</div>
						<div class="form-group">
							<p>Location</p>
							<input type="text"  name="location" class="form-control">
						</div>
						<div class="form-group">
							<p>Team</p>
							<input type="text" name="team" class="form-control">
						</div>
						<div class="form-group">
							<p>Photographer</p>
							<input type="text"  name="photographer" class="form-control">
						</div>
						-->
						<div class="form-group">
							<p>All Image</p>
							<textarea name="all_image" class="form-control" id="editor1"></textarea>
						</div>
						<input type="hidden" name="journal"  value="1">
						<!--
						<div class="form-group">
							<p>Awards</p>
							<textarea id="editor2" name="awards" class="form-control"></textarea>
						</div>
						-->
						<div class="form-group">
							<p>Meta Title  <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<input type="text" id="meta_title" name="meta_title" class="form-control">
						</div>
						<div class="form-group">
							<p>Meta Keywords  <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<input type="text" id="meta_keywords" name="meta_keywords" class="form-control">
						</div>
						<div class="form-group">
							<p>Meta Description  <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<textarea id="meta_description" name="meta_description" class="form-control"></textarea>
						</div>
						
					</div>
					
				</div>
				<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>			
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	 
</body>
</html>

<?php
	}
?>

