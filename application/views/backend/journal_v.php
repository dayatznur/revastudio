<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Journal <small>All File</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> Journal
                            </li>
                        </ol>
                    </div>
                </div>
				
				<p>
					<div align="right">
						<a href="<?php echo base_url(); ?>page/create_journal">
							<button type="button" class="btn btn-success">Create</button>
						</a>
					</div>
				</p>
				
				<div class="row">
					<div class="col-lg-12">
                        <div class="table-responsive">
                           <table class="table table-bordered table-hover table-striped" style="font-size:13px;">
                                <thead>
                                    <tr>
                                        
										<th>Projects Category</th>
                                        <th>Title</th>
										<th>Image</th>
										<th>Publish</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="daftar">
								<?php
								
								$query = mysql_query("select a.id_journal,a.id_projects_category,a.title_projects,a.image_projects,a.journal,a.order_no,
								b.id_projects_category,b.title_projects_category
								from journal a
								LEFT JOIN projects_category b on a.id_projects_category = b.id_projects_category
								order by a.order_no ASC");
								while($q=mysql_fetch_array($query)){
									?>
									<tr id="<?php echo "order_no_".$q['id_journal'] ?>">
										<td><?php echo $q['title_projects_category'] ?></td>
										<td><?php echo $q['title_projects'] ?></td>
										<td><img src="<?php echo base_url(); ?>all_picture/journal/small/<?php echo $q['image_projects'] ?>"></td>
										<td>
												<?php
													if($q['journal'] != 1){
														echo"Yes";
													}else{
														echo"No";
													}
												?>
											</td>
										<td>
											<a href="<?php echo base_url(); ?>page/journal_update/<?php echo $q['id_journal'] ?>">
												<button type="button" class="btn btn-sm btn-primary">&nbsp;&nbsp;&nbsp;<i class="fa fa-edit"></i> Edit&nbsp;&nbsp;&nbsp;</button>
											</a>
											<a href="<?php echo base_url(); ?>page/journal_delete/<?php echo $q['id_journal'] ?>" onclick = "return confirm('Are you sure you want to delete?')">
												<button type="button" class="btn btn-sm btn-info"><i class="fa fa-remove"></i> Delete</button>
											</a>
										</td>
									</tr>
									<?php
									
								}
								?>		
								</tbody>
                            </table>
                        </div>
                    </div>
				</div>
				
            </div>
        </div>
		
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>	
	
	<script>
		$(document).ready(function(){
                    $('.daftar').sortable({ 
                       cursor:'move',
                       update:function(){
                            var data_list = $('.daftar').sortable('serialize'); 
                            $.ajax({ 
                                type:'POST',
                                url:"<?php echo base_url(); ?>order_journal.php",
                                data: data_list
                            });
                       }
                    });
                 });
	</script>


</body>
</html>
<?php
	}
?>
