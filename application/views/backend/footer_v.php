 <!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datepicker.js"></script>

<script language="javascript">
	$(function(){	
		$('#dp1').datepicker({
			format: 'yyyy-mm-dd',
            todayBtn: 'linked'
		});
	});

	function ValidationUsers() {
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;
		var name = document.getElementById("name").value;
		var email = document.getElementById("email").value;
		if (username != '' && password != '' && name != '' && email != '') {
			if ((email.indexOf('@',0)==-1) || (email.indexOf('.',0)==-1)){
				$("#errorEmail").modal('show');
				return false;
			}
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationSeo() {
		var menu = document.getElementById("menu").value;
		var title = document.getElementById("title").value;
		var keyword = document.getElementById("keyword").value;
		var description = document.getElementById("description").value;
		if (menu != '' && title != '' && keyword != '' && description != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationBanner() {
		var title_banner = document.getElementById("title_banner").value;
		var gambar = document.getElementById("gambar").value;
		if (title_banner != '' && gambar != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationBannerUpdate() {
		var title_banner = document.getElementById("title_banner").value;
		if (title_banner != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationProjectCategory() {
		var title_projects_category = document.getElementById("title_projects_category").value;
		if (title_projects_category != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationProjects() {
		var projects_category = document.getElementById("projects_category").value;
		var title_projects = document.getElementById("title_projects").value;
		var gambar = document.getElementById("gambar").value;
		var meta_title = document.getElementById("meta_title").value;
		var meta_keywords = document.getElementById("meta_keywords").value;
		var meta_description = document.getElementById("meta_description").value;
		
		if (projects_category != '' && title_projects != '' && gambar != '' && meta_title != '' && meta_keywords != '' && meta_description != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationProjectsUpdate() {
		var projects_category = document.getElementById("projects_category").value;
		var title_projects = document.getElementById("title_projects").value;
		var meta_title = document.getElementById("meta_title").value;
		var meta_keywords = document.getElementById("meta_keywords").value;
		var meta_description = document.getElementById("meta_description").value;
		
		
		
		if (projects_category != '' && title_projects != ''  && meta_title != '' && meta_keywords != '' && meta_description != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationNewsCategory() {
		var title_news_category = document.getElementById("title_news_category").value;
		
		if (title_news_category != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationNews() {
		var news_category = document.getElementById("news_category").value;
		var title_news = document.getElementById("title_news").value;
		var gambar = document.getElementById("gambar").value;
		var meta_title = document.getElementById("meta_title").value;
		var meta_keywords = document.getElementById("meta_keywords").value;
		var meta_description = document.getElementById("meta_description").value;
		
		if (news_category != '' && title_news != '' && gambar != '' && meta_title != '' && meta_keywords != '' && meta_description != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationNewsUpdate() {
		var news_category = document.getElementById("news_category").value;
		var title_news = document.getElementById("title_news").value;
		var meta_title = document.getElementById("meta_title").value;
		var meta_keywords = document.getElementById("meta_keywords").value;
		var meta_description = document.getElementById("meta_description").value;
		
		if (news_category != '' && title_news != '' && meta_title != '' && meta_keywords != '' && meta_description != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationSocialMedia() {
		var title_social_media = document.getElementById("title_social_media").value;
		var link_social_media = document.getElementById("link_social_media").value;
		var gambar = document.getElementById("gambar").value;
		
		if (title_social_media != '' && link_social_media != '' && gambar != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationSocialMediaUpdate() {
		var title_social_media = document.getElementById("title_social_media").value;
		var link_social_media = document.getElementById("link_social_media").value;
		
		if (title_social_media != '' && link_social_media != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationContact() {
		var nation = document.getElementById("nation").value;
		var address = document.getElementById("address").value;
		var telp = document.getElementById("telp").value;
		var fax = document.getElementById("fax").value;
		var email = document.getElementById("email").value;
		var map = document.getElementById("map").value;
		
		if (nation != '' && address != '' && telp != '' && fax != '' && email != '' && map != '') {
			if ((email.indexOf('@',0)==-1) || (email.indexOf('.',0)==-1)){
				$("#errorEmail").modal('show');
				return false;
			}
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationCareers() {
		var position = document.getElementById("position").value;
		
		
		if (position != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationAbout() {
		var title = document.getElementById("title").value;
		var gambar = document.getElementById("gambar").value;
		if (title != '' && gambar != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationAboutUpdate() {
		var title = document.getElementById("title").value;
		if (title != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	function ValidationServices() {
		var title = document.getElementById("title").value;
		if (title != '' && gambar != '') {
			return true;
		} else{
			$("#error").modal('show');
			return false;
		}	
	}
	</script>

<!-- Error -->
<div id="error" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<strong>NOTICE</strong> <p>Data can not be saved. Please fill in all the data !</p>		
		</div>
	</div>
</div>
<div id="errorEmail" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<strong>NOTICE</strong> <p>There are errors in the writing of your email</p>		
		</div>
	</div>
</div>




<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url(); ?>assets/backend/js/plugins/morris/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/plugins/morris/morris.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/plugins/morris/morris-data.js"></script>

<!-- Editor -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckfinder/ckfinder.js"></script>

<script type="text/javascript">	
CKEDITOR.replace( 'editor',
	{
	skin : 'moono',
	toolbar : 'MyToolbar', 
	width:"100%",
	filebrowserBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Flash',
	});
</script>
<script type="text/javascript">	
CKEDITOR.replace( 'editor1',
	{
	skin : 'moono',
	toolbar : 'MyToolbar', 
	width:"100%",
	filebrowserBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Flash',
	});
</script>
<script type="text/javascript">	
CKEDITOR.replace( 'editor2',
	{
	skin : 'moono',
	toolbar : 'MyToolbar', 
	width:"100%",
	filebrowserBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : '<?php echo base_url();?>assets/ckfinder/ckfinder.html?type=Flash',
	});
</script>
