<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
    
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
               <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Blogs Category <small>Update</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                 <i class="fa fa-fw fa-file"></i> Category
                            </li>
                        </ol>
                    </div>
                </div>
				
				<div class="row">
					<?php foreach($blogs_category_update as $data): ?>
					<?php echo form_open('page/blogs_category_update_process/'.$data->id_blogs_category,'onsubmit="return ValidationblogsCategory()"'); ?>
					<div class="col-lg-8">
						<div class="form-group">
							<p>Title</p>
							<input type="text" id="title_blogs_category" name="title_blogs_category" value="<?php echo $data->title_blogs_category ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Publish</p>
							<?php
								if($data->publish == 1){
									?>
										<input type="radio" name="publish" value="1" checked> Yes 
										&nbsp;
										<input type="radio" name="publish" value="2"> No
									<?php 
								}else{
									?>
										<input type="radio" name="publish" value="1"> Yes 
										&nbsp;
										<input type="radio" name="publish" value="2" checked> No
									<?php 
								}
							?>
						</div>
						<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
						</form>		
					</div>
				</div>
				<?php endforeach ?>		
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	
</body>
</html>
<?php
	}
?>

