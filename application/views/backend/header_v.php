<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/backend/css/sb-admin.css" rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="<?php echo base_url(); ?>assets/backend/css/plugins/morris.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="<?php echo base_url(); ?>assets/backend/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
<link href="<?php echo base_url(); ?>assets/backend/css/datepicker.css" rel="stylesheet">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<!--
		<a class="navbar-brand" href="index.html">
			<img src="<?php echo base_url(); ?>assets/images/logo.png" id="iconLogo">
		</a>
		-->
	</div>
	<!-- Top Menu Items -->
	<ul class="nav navbar-right top-nav">
		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata('name'); ?> <b class="caret"></b></a>
			<ul class="dropdown-menu">
				<li>
					<a href="<?php echo base_url(); ?>page/myaccount"><i class="fa fa-fw fa-user"></i> Profile</a>
				</li>
				<li>
					<a href="<?php echo base_url(); ?>page/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
				</li>
			</ul>
		</li>
	</ul>
	<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav side-nav">
			<li class="active">
				<a href="<?php echo base_url(); ?>page/home"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
			</li>
			
			
			
			<li>
				<a href="javascript:;" data-toggle="collapse" data-target="#content"><i class="fa fa-fw fa-file"></i> Manage Content <i class="fa fa-fw fa-caret-down"></i></a>
				<ul id="content" class="collapse">
					<li>
						<a href="<?php echo base_url(); ?>page/about">About</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/banner">Banner</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/careers">Careers</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/designers">Designers</a>
					</li>

					<li>
						<a href="<?php echo base_url(); ?>page/logo">Logo</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/projects_category">Projects Category</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/projects">Projects</a>
					</li>
<li>
						<a href="<?php echo base_url(); ?>page/journal">Journal</a>
					</li>
					<!--
					<li>
						<a href="<?php echo base_url(); ?>page/services">Services</a>
					</li>
					-->
					<li>
						<a href="<?php echo base_url(); ?>page/news_category">News Category</a>
					</li>
					
					<li>
						<a href="<?php echo base_url(); ?>page/news">News</a>
					</li>

					<li>
						<a href="<?php echo base_url(); ?>page/blogs_category">Blogs Category</a>
					</li>
					
					<li>
						<a href="<?php echo base_url(); ?>page/blogs">Blogs</a>
					</li>

					<li>
						<a href="<?php echo base_url(); ?>page/social_media">Social Media</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/contact">Contact</a>
					</li>
				</ul>
			</li>

<?php 
			
				if($this->session->userdata('level') == 1){
					?>
					<li>
						
						<a href="<?php echo base_url(); ?>page/users"><i class="fa fa-fw fa-users"></i> Manege Users</a>
					</li>
					<li>
						<a href="<?php echo base_url(); ?>page/seo"> <i class="fa fa-fw fa-cogs"></i>  SEO Page</a>
					</li>
					<?php 
				}
			?>
		</ul>
	</div>
	<!-- /.navbar-collapse -->
</nav>

