<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Designers <small>All File</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> Designers
                            </li>
                        </ol>
                    </div>
                </div>
				
				<p>
					<div align="right">
						<a href="<?php echo base_url(); ?>page/create_designers">
							<button type="button" class="btn btn-success">Create</button>
						</a>
					</div>
				</p>
				
				<div class="row">
					<div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" style="font-size:13px;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Description</th>
										<th>Image</th>
										<th style="width:200px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php $i=1 ?>
									<?php foreach($designers as $data): ?>
										<tr>
											<td><?php echo"".$i++."" ?></td>
											<td><?php echo $data->title ?></td>
											<td><?php echo $data->description ?></td>
											<td><img src="<?php echo base_url(); ?>all_picture/designers/<?php echo $data->image_designers ?>" style="width: 200px;"></td>
											<td>
												<a href="<?php echo base_url(); ?>page/designers_update/<?php echo $data->id_designers ?>">
													<button type="button" class="btn btn-sm btn-primary">&nbsp;&nbsp;&nbsp;<i class="fa fa-edit"></i> Edit&nbsp;&nbsp;&nbsp;</button>
												</a>
												<a href="<?php echo base_url(); ?>page/designers_delete/<?php echo $data->id_designers ?>" onclick = "return confirm('Are you sure you want to delete?')">
													<button type="button" class="btn btn-sm btn-info"><i class="fa fa-remove"></i> Delete</button>
												</a>
											</td>
										</tr>
									<?php endforeach ?>
                              
                                </tbody>
                            </table>
                        </div>
                    </div>
				</div>
				
            </div>
        </div>
		
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>

</body>
</html>
<?php
	}
?>
