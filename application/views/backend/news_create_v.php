<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
   
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            News <small>Create</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> News
                            </li>
                        </ol>
                    </div>
                </div>
				
				
				
				<?php echo form_open_multipart('page/create_news_process', 'onsubmit="return ValidationNews()"'); ?>
				<div class="row">
					
					<div class="col-lg-8">
						
				
						<div class="form-group">
						<p>News Category</p>
						<select id="news_category"  name="news_category" style="margin-top:10px;" class="form-control">
							<option value="">- Select -</option>
							<?php foreach($news_category as $data): ?>
							
								<option value=<?php echo $data->id_news_category ?>><?php echo $data->title_news_category ?></option>
									
							<?php endforeach ?>		 
						</select>
						</div>
					
						<div class="form-group">
							<p>Title</p>
							<input type="text" id="title_news" name="title_news" class="form-control">
						</div>
						<div class="form-group">
							<p>Image</p>
							<div class="alert alert-info" style="padding:8px;">
							  <strong>Info!</strong> Max size 2 MB and format file jpg or png
							</div>
							<input type="file" name="gambar" id="gambar">
						</div>
						<div class="form-group">
							<p>Description</p>
							<textarea name="description" class="form-control" id="editor"></textarea>
						</div>
						
						<div class="form-group">
							<p>Meta Title</p>
							<input type="text" id="meta_title"name="meta_title" class="form-control">
						</div>
						<div class="form-group">
							<p>Meta Keywords</p>
							<input type="text" id="meta_keywords" name="meta_keywords" class="form-control">
						</div>
						<div class="form-group">
							<p>Meta Description</p>
							<textarea id="meta_description" name="meta_description" class="form-control"></textarea>
						</div>
						
					</div>
					
				</div>
				<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>			
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	 
</body>
</html>

<?php
	}
?>

