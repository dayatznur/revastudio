<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">
		<section class="main-news">
			<section class="content-news">
				<h3>PUBLICATION 1 TITLE</h3>
				<p>5 February 2016</p>
				<section><a href="<?php echo base_url(); ?>publication/details"><img src="<?php echo base_url(); ?>assets/img/building-view-corner.jpg" alt="building-view-corner"></a></section>
				<div class="read_more">
					<a href="<?php echo base_url(); ?>publication/details">Read More</a>
				</div>
			</section>
			<section class="content-news">
				<h3>PUBLICATION 2 TITLE</h3>
				<p>5 February 2016</p>
				<section><a href="<?php echo base_url(); ?>publication/details"><img src="<?php echo base_url(); ?>assets/img/building-view-corner.jpg" alt="building-view-corner"></a></section>
				<div class="read_more">
					<a href="<?php echo base_url(); ?>publication/details">Read More</a>
				</div>
			</section>
			<section class="content-news">
				<h3>PUBLICATION 3 TITLE</h3>
				<p>5 February 2016</p>
				<section><a href="<?php echo base_url(); ?>publication/details"><img src="<?php echo base_url(); ?>assets/img/building-view-corner.jpg" alt="building-view-corner"></a></section>
				<div class="read_more">
					<a href="<?php echo base_url(); ?>publication/details">Read More</a>
				</div>
			</section>
		</section>
	</article>
	
	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>