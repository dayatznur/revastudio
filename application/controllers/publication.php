<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publication extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index()
	{
		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('publication_v', $data);
	}

	function details(){

		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('publication_details_v', $data);	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */